# WP-test

This project provides a RESTful API that allows merchant to create, read, update and delete offers.

### Endpoints
    GET       /offers         find all offers
    POST      /offers         save offer (creating a new offer)
    GET       /offers/{id}    find one offer
    PUT       /offers/{id}    save offer (replace the offer with id {id})
    PATCH     /offers/{id}    save offer (replace partial information of the offer)
    DELETE    /offers/{id}    delete offer (cancel the offer)

### Run
This project uses [Maven](https://maven.apache.org/) for dependency management and build. To
build the project:
```bash
mvn packages
``` 
Once the above command complete successfully, you can find a `jar` file in the following path:
```bash
target/wp-test-0.1.0.jar
```
To start the server:
```bash
java -jar target/wp-test-0.1.0.jar
```
Then you can start accessing the server through `http` calls, e.g.:
```bash
GET http://localhost:8080/offers
```
    
### Implementation
The implementation is based on the [Spring Data Rest](https://spring.io/projects/spring-data-rest).
The persistent layer consists of an embedded, in-memory database called [H2 Database Engine](http://www.h2database.com/html/main.html).
   

