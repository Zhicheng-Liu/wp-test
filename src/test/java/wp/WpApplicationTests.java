package wp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.util.NestedServletException;
import wp.persistence.entities.Offer;
import wp.persistence.repositories.OfferRepository;

import java.sql.Timestamp;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureJsonTesters
@AutoConfigureMockMvc
public class WpApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private OfferRepository offerRepository;

    @Autowired
    private JacksonTester<Offer> offerJacksonTester;

    @Before
    public void deleteAllBeforeTest() throws Exception {
        offerRepository.deleteAll();
    }

    private String postTestOffer() throws Exception {
        return postOffer("productSKU",
                        "description",
                        Offer.Currency.GBP,
                        2.50D,
                        new Timestamp(System.currentTimeMillis()),
                        new Timestamp(System.currentTimeMillis()+1000*60*60*24));
    }

    private String postOffer(String productSKU,
                             String description,
                             Offer.Currency priceInCurrency,
                             Double offerPrice,
                             Timestamp offerStartDate,
                             Timestamp offerEndDate) throws Exception {
        Offer offer = new Offer(productSKU, description, priceInCurrency, offerPrice, offerStartDate, offerEndDate);

        MvcResult mvcResult = mockMvc.perform(post("/offers")
                .content(offerJacksonTester.write(offer).getJson()))
                .andExpect(status().isCreated())
                .andReturn();

        return mvcResult.getResponse().getHeader("Location");
    }

    @Test
    public void offerCanBeCreated() throws Exception {
        postTestOffer();
    }

    @Test
    public void offerCanBeShared() throws Exception {
        String offerUrl = postTestOffer();

        mockMvc.perform(get(offerUrl))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.productSKU").value("productSKU"));
    }

    @Test
    public void offerIsTimeBounded() throws Exception {
        String offerUrl = postTestOffer();

        mockMvc.perform(get(offerUrl))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.productSKU").value("productSKU"));

        mockMvc.perform(patch(offerUrl)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{" +
                        "\"offerEndDate\" : \"" + (new Timestamp(System.currentTimeMillis())).toInstant() +
                        "\" }"))
                .andExpect(status().is2xxSuccessful());

        // The offer is ended and expire automatically
        mockMvc.perform(get(offerUrl))
                .andExpect(status().isNotFound());
    }

    @Test
    public void offerCanBeCancelled() throws Exception {
        String offerUrl = postTestOffer();

        mockMvc.perform(get(offerUrl))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.productSKU").value("productSKU"));

        mockMvc.perform(delete(offerUrl))
                .andExpect(status().is2xxSuccessful());

        mockMvc.perform(get(offerUrl))
                .andExpect(status().isNotFound());
    }

    @Test(expected = NestedServletException.class)
    public void offerWithStartTimeAfterEndTimeShouldFail() throws Exception {
        Offer offer = new Offer("productSKU",
                                "description",
                                Offer.Currency.GBP,
                                2.50D,
                                new Timestamp(System.currentTimeMillis()),
                                new Timestamp(System.currentTimeMillis()-1000*60*60*24));

        mockMvc.perform(post("/offers").content(offerJacksonTester.write(offer).getJson()));
    }

    @Test(expected = NestedServletException.class)
    public void offerWithoutProductSKUShouldFail() throws Exception {
        Offer offer = new Offer("",
                                "description",
                                Offer.Currency.GBP,
                                2.50D,
                                new Timestamp(System.currentTimeMillis()),
                                new Timestamp(System.currentTimeMillis()+1000*60*60*24));

        mockMvc.perform(post("/offers").content(offerJacksonTester.write(offer).getJson()));
    }

    @Test(expected = NestedServletException.class)
    public void offerWithoutDescriptionShouldFail() throws Exception {
        Offer offer = new Offer("productSKU",
                "",
                Offer.Currency.GBP,
                2.50D,
                new Timestamp(System.currentTimeMillis()),
                new Timestamp(System.currentTimeMillis()+1000*60*60*24));

        mockMvc.perform(post("/offers").content(offerJacksonTester.write(offer).getJson()));
    }

    @Test(expected = NestedServletException.class)
    public void offerWithoutCurrencyShouldFail() throws Exception {
        Offer offer = new Offer("productSKU",
                "description",
                null,
                2.50D,
                new Timestamp(System.currentTimeMillis()),
                new Timestamp(System.currentTimeMillis()+1000*60*60*24));

        mockMvc.perform(post("/offers").content(offerJacksonTester.write(offer).getJson()));
    }

    @Test(expected = NestedServletException.class)
    public void offerWithoutPriceShouldFail() throws Exception {
        Offer offer = new Offer("productSKU",
                "description",
                Offer.Currency.GBP,
                null,
                new Timestamp(System.currentTimeMillis()),
                new Timestamp(System.currentTimeMillis()+1000*60*60*24));

        mockMvc.perform(post("/offers").content(offerJacksonTester.write(offer).getJson()));
    }

    @Test(expected = NestedServletException.class)
    public void offerWithoutStartDateShouldFail() throws Exception {
        Offer offer = new Offer("productSKU",
                "description",
                Offer.Currency.GBP,
                2.50D,
                null,
                new Timestamp(System.currentTimeMillis()+1000*60*60*24));

        mockMvc.perform(post("/offers").content(offerJacksonTester.write(offer).getJson()));
    }

    @Test(expected = NestedServletException.class)
    public void offerWithoutEndDateShouldFail() throws Exception {
        Offer offer = new Offer("productSKU",
                "description",
                Offer.Currency.GBP,
                2.50D,
                new Timestamp(System.currentTimeMillis()),
                null);

        mockMvc.perform(post("/offers").content(offerJacksonTester.write(offer).getJson()));
    }

}
