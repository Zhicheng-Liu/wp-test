package wp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WpApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(WpApplication.class, args);
    }
}
