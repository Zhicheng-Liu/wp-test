package wp.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = OfferStartTimeBeforeEndTimeValidator.class)
public @interface OfferStartTimeBeforeEndTime {

    String message() default "{constraints.offerStartTimeBeforeEndTime}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
