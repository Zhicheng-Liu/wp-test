package wp.validation;

import wp.persistence.entities.Offer;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class OfferStartTimeBeforeEndTimeValidator implements ConstraintValidator<OfferStartTimeBeforeEndTime, Offer> {

    @Override
    public boolean isValid(Offer offer, ConstraintValidatorContext constraintValidatorContext) {
        if ( offer.getOfferStartDate().compareTo(offer.getOfferEndDate()) < 0 ) {
            return true;
        }

        return false;
    }
}
