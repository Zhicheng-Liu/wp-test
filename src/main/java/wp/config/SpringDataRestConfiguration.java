package wp.config;

import wp.aspect.OfferTimeBoundAspect;
import wp.persistence.entities.Offer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;

@Configuration
@EnableJpaAuditing
public class SpringDataRestConfiguration {

    @Bean
    public RepositoryRestConfigurer repositoryRestConfigurer() {
        return new RepositoryRestConfigurerAdapter() {

            @Override
            public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
                config.getCorsRegistry().addMapping("/**").allowedMethods("*").allowedOrigins("*");
                config.exposeIdsFor(Offer.class);
            }

        };
    }

    @Bean
    public OfferTimeBoundAspect offerTimeBoundAspect() {
        return new OfferTimeBoundAspect();
    }
}
