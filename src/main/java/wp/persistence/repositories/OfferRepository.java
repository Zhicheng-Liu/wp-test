package wp.persistence.repositories;

import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import wp.persistence.entities.Offer;
import org.springframework.data.repository.PagingAndSortingRepository;

@RepositoryRestResource
public interface OfferRepository extends PagingAndSortingRepository<Offer, Long> {
}
