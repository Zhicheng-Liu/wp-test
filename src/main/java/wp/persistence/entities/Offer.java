package wp.persistence.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import wp.validation.OfferStartTimeBeforeEndTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Timestamp;

@Entity
@OfferStartTimeBeforeEndTime
public class Offer extends Auditable {

    public enum Currency {
        GBP,
        USD
    }

    @ApiModelProperty(position = 1, value = "Offer auto generated id", required = true, readOnly = true)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @ApiModelProperty(position = 2, value = "The SKU of the product this offer is related to", required = true)
    @JsonProperty
    @Size(min = 1, max = 255)
    @NotNull(message = "Product SKU can not be NULL.")
    @Column(nullable = false)
    private String productSKU;

    @ApiModelProperty(position = 3, value = "Shopper friendly description", required = true)
    @JsonProperty
    @Size(min = 1, max = 255)
    @NotNull(message = "Offer description can not be NULL.")
    @Column(nullable = false)
    private String description;

    @ApiModelProperty(position = 4, value = "The currency the offer price is in", required = true)
    @JsonProperty
    @Enumerated(EnumType.STRING)
    @NotNull(message = "Offer price currency can not be NULL.")
    @Column(nullable = false)
    private Currency priceInCurrency;

    @ApiModelProperty(position = 5, value = "The offer price", required = true)
    @JsonProperty
    @NotNull(message = "Offer price can not be NULL.")
    @Column(nullable = false)
    private Double offerPrice;

    @ApiModelProperty(position = 6, value = "The offer start time", required = true)
    @JsonProperty
    @NotNull(message = "Offer start date can not be NULL.")
    @Column(nullable = false)
    private Timestamp offerStartDate;

    @ApiModelProperty(position = 7, value = "The offer end time", required = true)
    @JsonProperty
    @NotNull(message = "Offer end date can not be NULL.")
    @Column(nullable = false)
    private Timestamp offerEndDate;

    public Offer() {}

    public Offer(String productSKU,
                 String description,
                 Currency priceInCurrency,
                 Double offerPrice,
                 Timestamp offerStartDate,
                 Timestamp offerEndDate) {
        this.productSKU = productSKU;
        this.description = description;
        this.priceInCurrency = priceInCurrency;
        this.offerPrice = offerPrice;
        this.offerStartDate = offerStartDate;
        this.offerEndDate = offerEndDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getProductSKU() {
        return productSKU;
    }

    public void setProductSKU(String productSKU) {
        this.productSKU = productSKU;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Currency getPriceInCurrency() {
        return priceInCurrency;
    }

    public void setPriceInCurrency(Currency priceInCurrency) {
        this.priceInCurrency = priceInCurrency;
    }

    public Double getOfferPrice() {
        return offerPrice;
    }

    public void setOfferPrice(Double offerPrice) {
        this.offerPrice = offerPrice;
    }

    public Timestamp getOfferStartDate() {
        return offerStartDate;
    }

    public void setOfferStartDate(Timestamp offerStartDate) {
        this.offerStartDate = offerStartDate;
    }

    public Timestamp getOfferEndDate() {
        return offerEndDate;
    }

    public void setOfferEndDate(Timestamp offerEndDate) {
        this.offerEndDate = offerEndDate;
    }
}
