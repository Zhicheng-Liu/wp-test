package wp.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import wp.persistence.entities.Offer;

import java.sql.Timestamp;
import java.util.Optional;

/**
 * An @Aspect for ensuring only valid offers are returned through checking an offer's start and end time
 */
@Aspect
public class OfferTimeBoundAspect {

    /**
     * An @Around advice for CrudRepository.findOne(..) method execution
     *
     * It takes the returned object from join point method execution and check the returned object. If the returned
     * object is not an Offer object, return as it is. If the returned object is an Offer object, check the offer's
     * offerStartDate and offerEndDate fields. Return null if the current time is not between these two.
     *
     * @param proceedingJoinPoint
     * @return null or object
     * @throws Throwable
     */
    @Around("execution(* wp.persistence.repositories.OfferRepository.find*(..))")
    public Object filterOnOfferValidity(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        Object result = proceedingJoinPoint.proceed();

        if ( result == null ) {
            return null;
        }

        if ( result instanceof Optional && ((Optional) result).isPresent() ) {
            Offer offer = (Offer) ((Optional) result).get();
            Timestamp now = new Timestamp(System.currentTimeMillis());
            if ( offer.getOfferStartDate().after(now) || offer.getOfferEndDate().before(now) ) {
                return Optional.empty();
            }
        }

        return result;
    }
}
